import random

def calculer_imc(taille, poids):
    return poids / (taille * taille)

def est_en_surpoids(sexe, age, imc):
    if sexe == "homme":
        if age < 45:
            if imc >= 25:
                return True
        elif age >= 45:
            if imc >= 27:
                return True
    elif sexe == "femme":
        if age < 45:
            if imc >= 22:
                return True
        elif age >= 45:
            if imc >= 25:
                return True
    return False

personnes = []

for i in range(10):
    personne = {}
    personne["nom"] = "Personne " + str(i+1)
    personne["age"] = random.randint(6, 65)
    personne["sexe"] = random.choice(["masculin", "féminin"])
    personne["taille"] = random.uniform(1.20, 2.0)
    personne["poids"] = random.randint(20, 100)
    personne["imc"] = calculer_imc(personne["taille"], personne["poids"])
    personne["en_surpoids"] = est_en_surpoids(personne["sexe"], personne["age"], personne["imc"])
    personnes.append(personne)

for personne in personnes:
    print("Nom :", personne["nom"])
    print("Age :", personne["age"])
    print("Sexe :", personne["sexe"])
    print("Taille :", personne["taille"])
    print("IMC :", personne["imc"])
    print("En surpoids :", "Oui" if personne["en_surpoids"] else "Non")
    print()
